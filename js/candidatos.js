const candidatos = [
    {
        numero: "1300",
        nome: "Luis Inacio Lula da Silva",
        partido: "PT",
        foto: "https://eleicoes.poder360.com.br/media/fotos/2018/BR/280000625869.jpg",
        votos: 0
    },
    {
        numero: "6487",
        nome: "Tom Hanks",
        partido: "PFG",
        foto: "https://upload.wikimedia.org/wikipedia/commons/f/f0/TomHanksJan2009_cropped.jpg",
        votos: 0
    },
    {
        numero: "1555",
        nome: "Marilia Mendonça",
        partido: "PS",
        foto: "https://lh3.googleusercontent.com/NGptvS_xXgIt1dLhNSw35tH8zVI44fB_m14rx_KowNWlADH9J_QU-D8fBNn4WOzTWJZezg6LDpPl3bBvF0i1vqhy",
        votos: 0
    },
    {
        numero: "1900",
        nome: "Wagner Moura",
        partido: "PWM",
        foto: "https://www.ilo.org/wcmsp5/groups/public/---dgreports/---dcomm/documents/image/wcms_389173.jpg",
        votos: 0
    },
    {
        numero: "1622",
        nome: "Adam Sandler",
        partido: "PAS",
        foto: "https://static.wikia.nocookie.net/dublagempedia/images/1/15/Adam-sandler_sc_768x1024.png/revision/latest?cb=20181111163755&path-prefix=pt-br",
        votos: 0
    },
    {
        numero: "3777",
        nome: "Carmen Lúcia",
        partido: "PST",
        foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQK6g5h-0aGiqXpZvnH-yFGXC1d-cTp3NztDw&usqp=CAU",
        votos: 0
    },
    {
        numero: "2022",
        nome: "Neymar Júnior",
        partido: "POA",
        foto: "https://upload.wikimedia.org/wikipedia/commons/8/83/Bra-Cos_%281%29_%28cropped%29.jpg",
        votos: 0
    }
]