const votoNulo = {votos: 0}
const votoBranco = {votos: 0}


const apuracao = document.getElementById("apuracao")
const apuracaoBrancos = document.getElementById("votos-brancos")
const apuracaoNulos = document.getElementById("votos-nulos")

const botoesDigitos = document.getElementsByClassName("digito")
const campo_numeros_escolhidos = document.getElementById("campo-numeros-escolhidos")
const blockNumeros = document.getElementsByClassName("block-numero")
const foto = document.getElementById("foto")
const nome_candidato = document.getElementById("nome-candidato")
const partido_candidato = document.getElementById("partido-candidato")
const botaoCorrigir = document.getElementById("btn-corrigir")
const botaoConfirmar = document.getElementById("btn-confirmar")
const botaoBranco = document.getElementById("btn-branco")
const msgVotoSemCadidato = document.getElementById("voto-sem-candidato")

mostrarApuracao()

botaoCorrigir.addEventListener("click", limparTela)
botaoBranco.addEventListener("click", () => {
    limparTela()
    campo_numeros_escolhidos.classList.add("hidden")
    msgVotoSemCadidato.innerHTML = "Voto BRANCO"
    msgVotoSemCadidato.classList.remove("hidden")
})
botaoConfirmar.addEventListener("click", () => {
    if(!campo_numeros_escolhidos.classList.contains("hidden")){
        let notEscondidos = 0
        for(const blockNumero of blockNumeros){
            if(!blockNumero.classList.contains("hidden")) notEscondidos++
        }
        if(notEscondidos === blockNumeros.length){
            const digitos_escolhidos = buscarDigitos()
            const candidato = candidatos.find(candidato => candidato.numero === digitos_escolhidos)
            if(candidato) candidato.votos++
            else votoNulo.votos++
            const audio = new Audio("sound/Corte-Urna.mp3");
            audio.play()
            mostrarApuracao()
            limparTela()
        }else alert("Digite todos os campos!")
    }else{
        votoBranco.votos++
        const audio = new Audio("sound/Corte-Urna.mp3");
        audio.play()
        campo_numeros_escolhidos.classList.remove("hidden")
        mostrarApuracao()
        limparTela()
    }
})

for(const botaoDigito of botoesDigitos){
    botaoDigito.addEventListener("click",() => handleClickDigito(botaoDigito))
}

function handleClickDigito(botaoDigito){
    if(!campo_numeros_escolhidos.classList.contains("hidden")){
        let blocoVazio = blocoDisponivel()
        if(blocoVazio) {
            blocoVazio.classList.remove("hidden")
            blocoVazio.innerHTML = botaoDigito.value
        }
        if(!blockNumeros.item(blockNumeros.length-1).classList.contains("hidden")) mostrarCandidado()
    }
}

function blocoDisponivel(){
    for(let i=0; i<blockNumeros.length;i++){
        if(blockNumeros.item(i).classList.contains("hidden")) return blockNumeros.item(i)
    }
    return null
}

function mostrarCandidado(){
    const digitos_escolhidos = buscarDigitos()
    const candidato = candidatos.find(candidato => candidato.numero === digitos_escolhidos)
    if(candidato){
        foto.setAttribute("src",candidato.foto)
        foto.classList.remove("hidden")
        nome_candidato.innerHTML = "Nome: " + candidato.nome
        nome_candidato.classList.remove("hidden")
        partido_candidato.innerHTML = "Partido: " + candidato.partido
        partido_candidato.classList.remove("hidden")
    }else{
        msgVotoSemCadidato.innerHTML = "Voto NULO"
        msgVotoSemCadidato.classList.remove("hidden")
    }

}   

function buscarDigitos(){
    let soma = ""
    for(const blockNumero of blockNumeros){
       soma+=blockNumero.innerHTML
    }
    return soma
}

function mostrarApuracao(){
    apuracao.innerHTML = ""
    for(const candidato of candidatos){
        let tr = document.createElement("tr")
        let tdNumero = document.createElement("td")
        let tdNome = document.createElement("td")
        let tdPartido = document.createElement("td")
        let tdVotos = document.createElement("td")

        tdNumero.textContent = candidato.numero
        tr.appendChild(tdNumero)
        tdNome.textContent = candidato.nome
        tr.appendChild(tdNome)
        tdPartido.textContent = candidato.partido
        tr.appendChild(tdPartido)
        tdVotos.textContent = candidato.votos
        tr.appendChild(tdVotos)
        apuracao.appendChild(tr)
    }
    apuracaoBrancos.innerHTML = "Brancos: " + votoBranco.votos
    apuracaoNulos.innerHTML = "Nulos: " + votoNulo.votos

}

function limparTela(){
    msgVotoSemCadidato.classList.add("hidden")
    campo_numeros_escolhidos.classList.remove("hidden")
    for(const blockNumero of blockNumeros){
        blockNumero.classList.add("hidden")
    }
    foto.setAttribute("src", "")
    foto.classList.add("hidden")
    nome_candidato.classList.add("hidden")
    partido_candidato.classList.add("hidden")
}

